package com.locker.JNA;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;

public class Test1 {

	public interface Clibrary extends Library {
		
		// 判断平台，选择使用的本地类库。msvcrt即msvcrt.dll
		Clibrary INSTANCE = (Clibrary) Native.loadLibrary(Platform.isWindows() ? "msvcrt" : "c", Clibrary.class);
		
		// 使用msvcrt.dll这个C运行时库中的printf函数打印
		void printf(String format, Object... objects);
	}

	public static void main(String[] args) {
		Clibrary clibrary = Clibrary.INSTANCE;
		clibrary.printf("Hello /n");

		//System.out.println(args.length);
		
		for (int i = 0; i < args.length; i++) {
			clibrary.printf("Argument %d : %s/n", args);
		}
		
	}

}
