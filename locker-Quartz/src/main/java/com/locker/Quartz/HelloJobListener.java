package com.locker.Quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

/**
 * 监听
 * 
 * @author Locker <br>
 *         2017年2月21日 下午3:14:50
 * @version v1.0
 */
public class HelloJobListener implements JobListener {

	public static final String LISTENER_NAME = "dummyJobListenerName";

	@Override
	public String getName() {
		return LISTENER_NAME;
	}

	// Run this if job is about to be executed.
	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		String jobName = context.getJobDetail().getKey().toString();
		System.out.println("jobToBeExecuted");
		System.out.println("Job : " + jobName + " is going to start...");
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		System.out.println("jobExecutionVetoed");
	}

	// Run this after job has been executed
	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		System.out.println("jobWasExecuted");

		String jobName = context.getJobDetail().getKey().toString();
		System.out.println("Job : " + jobName + " is finished...");

		if (!jobException.getMessage().equals("")) {
			System.out.println("Exception thrown by: " + jobName + " Exception: " + jobException.getMessage());
		}
	}

}
