package com.locker.X.BASE64;

import org.junit.Test;

import com.locker.X.common.Common;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Base64加解密
 * @author Locker
 * 	  <br> 2017年2月20日 下午2:14:15
 * @version v1.0
 */
public class Base64 {
	
	/**
	 * 编码
	 */
	@Test
	public void encode() {
		BASE64Encoder enc = new BASE64Encoder();
		String encodeStr = enc.encode(Common.testByte);
		System.out.println(encodeStr);
	}
	
	/**
	 * 解码
	 * @throws Exception 
	 */
	@Test
	public void decode() throws Exception {
		String testStr = "U2hvdyBtZSB3aGF0IHlvdSB3YW50LCBjb21lIG9uLg==";
		BASE64Decoder dec = new BASE64Decoder();
		byte[] decodeBuffer = dec.decodeBuffer(testStr);
		String decodeStr = new String(decodeBuffer);
		System.out.println(decodeStr);
	}
	
}
