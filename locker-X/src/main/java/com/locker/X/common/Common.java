package com.locker.X.common;

/**
 * 通用
 * @author Locker
 * 	  <br> 2017年2月20日 下午2:10:31
 * @version v1.0
 */
public class Common {

	public static final String testStr = "Show me what you want, come on.";
	
	public static final byte[] testByte = testStr.getBytes();
	
}
