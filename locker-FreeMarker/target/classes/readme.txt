常用标签：
	1、list：该标签主要是进行迭代服务器端传递过来的List集合，比如：
		<#list nameList as names>    
		  ${names}   
		</#list>  
		name是list循环的时候取的一个循环变量，freemarker在解析list标签的时候，
		等价于：
			for (String names : nameList) {  
			    System.out.println(names);  
			}  
	2、if：    该标签主要是做if判断用的，比如：
		<#if (names=="陈靖仇")>  
		 他的武器是: 十五~~  
		</#if>  
		这个是条件判断标签，要注意的是条件等式必须用括号括起来， 等价于：
			if(names.equals("陈靖仇")){  
			    System.out.println("他的武器是: 十五~~");  
			}  
	3、include：该标签用于导入文件用的。
		<#include "include.html"/>  
		这个导入标签非常好用，特别是页面的重用。
	4、在静态文件中可以使用${} 获取值，取值方式和el表达式一样，非常方便。
	
自定义标签：
	  	以前写标签需要在<后加# ，但是freemarker要识别自定义标签需要在后面加上@，然后后面可以定义一些参数，
	  当程序执行template.process(paramMap, out);,就会去解析整个页面的所有的freemarker标签。
     	自定义标签 需要自定义一个类，然后实现TemplateDirectiveModel，重写execute方法，完成获取参数，根据参数do something等等。。
    	将自定义标签与解析类绑定在一起需要在paramMap中放入该解析类的实例，存放的key与自定义标签一致即可。。
   	 注意：在自定义标签中，如果标签内什么也没有，开始标签和结束标签绝对不能再同一行，不然会报错 freemarker.log.JDK14LoggerFactory$JDK14Logger error。

 java生成静态页面 http://blog.csdn.net/wangliqiang1014/article/details/20048629
