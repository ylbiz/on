package com.locker.FreeMarker;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import freemarker.core.Environment;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;

/**
 * 自定义标签
 * @author Locker
 * 	  <br> 2017年2月21日 上午9:19:21
 * @version v1.0
 */
public class ContentDirective implements TemplateDirectiveModel{

	private static final String PARAM_NAME = "name";
	private static final String PARAM_AGE = "age";

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {

		if (body == null) {
			throw new TemplateModelException("null body");
		} else {
			try {
				String name = getString(PARAM_NAME, params);
				Integer age = getInt(PARAM_AGE, params);

				if (name != null) {
					env.setVariable("output",
							DefaultObjectWrapper.DEFAULT_WRAPPER.wrap("从ContentDirective解析类中获得的参数为 name = " + name));
				}
				if (age != null) {
					env.setVariable("append", DefaultObjectWrapper.DEFAULT_WRAPPER.wrap("年龄 age = " + age));
				}

				Writer writer = env.getOut();
				writer.write("从这里输出可以再页面看到具体的内容，就像document.writer写入操作一样。<br/>");
				body.render(writer);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}
	
	/**
	 * 获取Map中String类型的参数值
	 * @param paramName
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public static String getString(String paramName, Map<String, TemplateModel> paramMap) throws Exception {
		TemplateModel model = paramMap.get(paramName);
		if (model == null) {
			return null;
		}
		if (model instanceof TemplateScalarModel) {
			return ((TemplateScalarModel) model).getAsString();
		} else if (model instanceof TemplateNumberModel) {
			return ((TemplateNumberModel) model).getAsNumber().toString();
		} else {
			throw new TemplateModelException("paramName");
		}
	}
	
	/**
	 * 获取int类型的参数
	 * @param paramName
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public static Integer getInt(String paramName, Map<String, TemplateModel> paramMap) throws Exception {
		TemplateModel model = paramMap.get(paramName);
		if (model == null) {
			return null;
		}
		if (model instanceof TemplateScalarModel) {
			String str = ((TemplateScalarModel) model).getAsString();
			try {
				return Integer.valueOf(str);
			} catch (NumberFormatException e) {
				throw new TemplateModelException(paramName);
			}
		} else if (model instanceof TemplateNumberModel) {
			return ((TemplateNumberModel) model).getAsNumber().intValue();
		} else {
			throw new TemplateModelException(paramName);
		}
	}

}
