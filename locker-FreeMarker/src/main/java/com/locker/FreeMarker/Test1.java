package com.locker.FreeMarker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

public class Test1 {

	// 文件夹路径
	public static final File file = new File("C:/Locker/project/maven/locker-FreeMarker/src/main/webapp/ftl");
	
	/**
	 * 使用路径创建模板
	 * @throws Exception
	 */
	@Test
	public void test1() throws Exception {
		@SuppressWarnings("deprecation")
		final Configuration cfg = new Configuration();
		
		// 设置加载模板的显式目录
		cfg.setDirectoryForTemplateLoading(file);
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("user", "张四");
		map.put("website", "csdn");
		map.put("url", "www.csdn.com");
		
		// 创建模板对象
		Template t = cfg.getTemplate("template1.ftl");
		
		// 在模板上执行插值操作，并输出到指定的输出流中
		t.process(map, new OutputStreamWriter(System.out));
		
	}
	
	/**
	 * 代码直接创建模板
	 * @throws Exception 
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void test2() throws Exception {
		StringBuilder html = new StringBuilder();
		html.append("${user}--${url}");
		
		StringReader reader = new StringReader(html.toString());
		Template template = new Template(null, reader);
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("user", "张四");
		map.put("website", "csdn");
		map.put("url", "www.csdn.com");
		
		template.process(map, new OutputStreamWriter(System.out));
	}
	
	/**
	 * fm标签示例
	 * @throws Exception 
	 */
	@Test
	public void test3() throws Exception {
		Configuration cfg = new Configuration();
		cfg.setDirectoryForTemplateLoading(file);
		cfg.setObjectWrapper(new DefaultObjectWrapper());
		// 设置编码格式
		cfg.setDefaultEncoding("UTF-8");
		// 创建模板对象
		Template t = cfg.getTemplate("template2.ftl");
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("description", "I am learning FreeMarker");
		
		List<String> nameList = new ArrayList<String>();
		nameList.add("陈靖仇");  
        nameList.add("玉儿");  
        nameList.add("宇文拓");
        paramMap.put("nameList", nameList);
        
        Map<String, Object> weaponMap = new HashMap<String, Object>();
        weaponMap.put("first", "轩辕剑");  
        weaponMap.put("second", "崆峒印");  
        weaponMap.put("third", "女娲石");  
        weaponMap.put("fourth", "神农鼎");  
        weaponMap.put("fifth", "伏羲琴");  
        weaponMap.put("sixth", "昆仑镜");  
        weaponMap.put("seventh", null);
        paramMap.put("weaponMap", weaponMap);
        
		Writer writer = new OutputStreamWriter(new FileOutputStream("new_template2.html"), "UTF-8");
		t.process(paramMap, writer);
		
		System.out.println("生成完成");
		
	}
	
	/**
	 * 自定义标签
	 * @throws Exception 
	 */
	@Test
	public void test4() throws Exception {
		Configuration cfg = new Configuration();
		cfg.setDirectoryForTemplateLoading(file);
		cfg.setObjectWrapper(new DefaultObjectWrapper());
		cfg.setDefaultEncoding("UTF-8");
		Template t = cfg.getTemplate("template3.ftl");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		// 创建自定义标签实现对象
		paramMap.put("content", new ContentDirective());
		
		t.process(paramMap, new OutputStreamWriter(System.out));
	}
	
}
